package com.josias.curso.dto;

import java.io.Serializable;

import com.josias.curso.domain.Produto;

public class ProdutoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nome;
	private double preco;
	
	public ProdutoDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ProdutoDTO(Produto dto) {
		this.id = dto.getId();
		this.nome = dto.getNome();
		this.preco = dto.getPreco();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	
	
}
