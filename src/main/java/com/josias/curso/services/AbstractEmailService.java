package com.josias.curso.services;

import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.josias.curso.domain.Cliente;
import com.josias.curso.domain.Pedido;

abstract public class AbstractEmailService implements EmailService {

	@Autowired
	private TemplateEngine engine;

	@Autowired
	private JavaMailSender javaSender;

	@Value("${default.sender}")
	private String sender;

	@Override
	public void sendOrderConfirmationEmail(Pedido pedido) {
		SimpleMailMessage sm = prepareSimpleMailMessage(pedido);
		sendEmail(sm);
	}

	protected String htmlFromTemplatePedido(Pedido obj) {
		Context context = new Context();
		context.setVariable("pedido", obj);
		return engine.process("email/confirmacaoPedido", context);
	}

	protected SimpleMailMessage prepareSimpleMailMessage(Pedido pedido) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(pedido.getCliente().getEmail());
		sm.setFrom(sender);
		sm.setSubject("Pedido Confirmado. Código: " + pedido.getId());
		sm.setText(pedido.toString());
		return sm;
	}

	@Override
	public void sendOrderConfimationHtmlEmail(Pedido pedido) {
		MimeMessage mm;
		try {
			mm = prepareHtmlMessage(pedido);
			sendHtmlEmail(mm);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			sendOrderConfirmationEmail(pedido);
		}
	}

	@Override
	public void sendNewPasswordEmail(Cliente cliente, String newPass) {
		SimpleMailMessage sm = prepareNewPasswordEmail(cliente, newPass);
		sendEmail(sm);
	}

	protected SimpleMailMessage prepareNewPasswordEmail(Cliente cliente, String newPass) {
		SimpleMailMessage sm = new SimpleMailMessage();
		sm.setTo(cliente.getEmail());
		sm.setFrom(sender);
		sm.setSubject("Solicitação de nova senha");
		sm.setSentDate(new Date(System.currentTimeMillis()));
		sm.setText("Nova senha: " + newPass);
		return sm;
	}

	protected MimeMessage prepareHtmlMessage(Pedido obj) throws MessagingException {
		MimeMessage mm = javaSender.createMimeMessage();
		MimeMessageHelper mh = new MimeMessageHelper(mm, true);
		mh.setTo(obj.getCliente().getEmail());
		mh.setFrom(sender);
		mh.setSubject("Pedido Confirmado. Código: " + obj.getId());
		mh.setSentDate(new Date(System.currentTimeMillis()));
		mh.setText(htmlFromTemplatePedido(obj), true);
		return mm;
	}
}
