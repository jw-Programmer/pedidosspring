package com.josias.curso.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.josias.curso.domain.PagamentoComBoleto;

@Service
public class BoletoService {
	public void preenchePagametoComBoleto(PagamentoComBoleto pagto, Date instanteOfPedido) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(instanteOfPedido);
		cal.add(Calendar.DAY_OF_MONTH, 7);
		pagto.setDataVencimento(cal.getTime());
	}
}
