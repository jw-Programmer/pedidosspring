package com.josias.curso.services;

import java.awt.image.BufferedImage;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.josias.curso.domain.Cidade;
import com.josias.curso.domain.Cliente;
import com.josias.curso.domain.Endereco;
import com.josias.curso.domain.enuns.Perfil;
import com.josias.curso.domain.enuns.TipoCliente;
import com.josias.curso.dto.ClienteDTO;
import com.josias.curso.dto.ClienteNewDTO;
import com.josias.curso.repositories.ClienteRepository;
import com.josias.curso.repositories.EnderecoRepository;
import com.josias.curso.security.UserSS;
import com.josias.curso.services.expetions.AuthorizationException;
import com.josias.curso.services.expetions.DataIntegrityException;
import com.josias.curso.services.expetions.ObjectNotFoundException;

@Service
public class ClienteService {

	@Value("${img.prefix.client.profile}")
	private String prefix;

	@Value("${img.profile.size}")
	private Integer size;

	@Autowired
	private ClienteRepository repo;

	@Autowired
	private EnderecoRepository endRepo;

	@Autowired
	private BCryptPasswordEncoder be;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private ImageService imageService;

	public Cliente find(Integer id) {
		UserSS user = UserService.authenticated();
		if (user == null || !user.asRole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado!!!");
		}
		Optional<Cliente> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Cliente não encontrada. id: " + id + " não existe"));
	}

	public List<Cliente> findAll() {
		return repo.findAll();
	}

	public Cliente FindByEmail(String email) {
		UserSS user = UserService.authenticated();
		if (user == null || !user.asRole(Perfil.ADMIN) && !email.equals(user.getUsername()))
			throw new AuthorizationException("Acesso negado!!");
		Cliente cli = repo.findByEmail(email);
		if (cli == null)
			throw new ObjectNotFoundException("Cliente com email não foi encontrado");
		return cli;

	}

	@Transactional
	public Cliente insert(Cliente obj) {
		obj.setId(null);
		obj = repo.save(obj);
		endRepo.saveAll(obj.getEnderecos());
		return obj;
	}

	public Cliente update(Cliente obj) {
		Cliente newObj = find(obj.getId());
		updateData(newObj, obj);
		return repo.save(newObj);
	}

	private void updateData(Cliente newObj, Cliente obj) {
		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir porque existem entidades relacionadas", e);
		}
	}

	public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}

	public Cliente fromDTO(ClienteDTO objDTO) {
		return new Cliente(objDTO.getId(), objDTO.getNome(), objDTO.getSenha(), objDTO.getEmail(), null, null);
	}

	public Cliente fromDTO(ClienteNewDTO objDTO) {
		Cliente cli = new Cliente(null, objDTO.getNome(), be.encode(objDTO.getSenha()), objDTO.getEmail(),
				objDTO.getCpfOuCnpj(), TipoCliente.toEnun(objDTO.getTipo()));
		Cidade cid = new Cidade(objDTO.getCidadeId(), null, null);
		Endereco end = new Endereco(null, objDTO.getLogradouro(), objDTO.getNumero(), objDTO.getComplemento(),
				objDTO.getBairro(), objDTO.getCep(), cli, cid);
		cli.getEnderecos().add(end);
		cli.getTelefones().add(objDTO.getTelefone1());
		if (objDTO.getTelefone2() != null) {
			cli.getTelefones().add(objDTO.getTelefone2());
		}

		return cli;
	}

	public URI uploadProfileFile(MultipartFile multiPartFile) {
		UserSS user = UserService.authenticated();
		if (user == null)
			throw new AuthorizationException("Não têm usuario logado");

		BufferedImage jpgImg = imageService.getJpgImageFromFile(multiPartFile);
		jpgImg = imageService.cropSquare(jpgImg);
		jpgImg = imageService.resize(jpgImg, size);
		String filename = prefix + user.getId() + ".jpg";
		URI uri = s3Service.uploudFile(imageService.getInputStream(jpgImg, "jpg"), filename, "image");

		return uri;
	}
}
