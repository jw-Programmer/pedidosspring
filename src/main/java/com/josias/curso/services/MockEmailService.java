package com.josias.curso.services;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;

public class MockEmailService extends AbstractEmailService {

	private static final Logger LOG = LoggerFactory.getLogger(MockEmailService.class);

	@Override
	public void sendEmail(SimpleMailMessage email) {
		LOG.info("Simulando servico de email");
		LOG.info(email.toString());
		LOG.info("Email Enviado");

	}

	@Override
	public void sendHtmlEmail(MimeMessage msg) {
		LOG.info("Simulando servico de email html");
		LOG.info(msg.toString());
		LOG.info("Email Enviado");
	}

}
