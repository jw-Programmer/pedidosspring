package com.josias.curso.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.josias.curso.services.expetions.FileException;

@Service
public class S3Service {
	private Logger LOG = LoggerFactory.getLogger(S3Service.class);

	@Autowired
	private AmazonS3 s3Client;

	@Value("${s3.bucket}")
	private String bucket;

	public URI uploudFile(MultipartFile multiPathFile) {
		try {
			String filename = multiPathFile.getOriginalFilename();
			InputStream is = multiPathFile.getInputStream();
			String contentType = multiPathFile.getContentType();
			return uploudFile(is, filename, contentType);
		} catch (IOException e) {
			throw new FileException("Erro no stream io");
		}

	}

	public URI uploudFile(InputStream is, String filename, String contentType) {
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType(contentType);
			LOG.info("uploud");
			s3Client.putObject(bucket, filename, is, metadata);
			return s3Client.getUrl(bucket, filename).toURI();
		} catch (URISyntaxException e) {
			throw new RuntimeException("Erro na conversão de uri");
		}

	}
}
