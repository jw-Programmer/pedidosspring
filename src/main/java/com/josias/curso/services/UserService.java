package com.josias.curso.services;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.josias.curso.security.UserSS;

@Service
public class UserService {
	public static UserSS authenticated() {
		try {
			return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			System.err.println("Problema no whoaiam");
			return null;
		}
	}
}
